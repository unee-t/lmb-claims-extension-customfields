# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# This Source Code Form is "Incompatible With Secondary Licenses", as
# defined by the Mozilla Public License, v. 2.0.

package Bugzilla::Extension::LMBCustomFields;

use 5.10.1;
use strict;
use warnings;
use Try::Tiny;

use parent qw(Bugzilla::Extension);

# This code for this is in ../extensions/LMBCustomFields/lib/Util.pm
use Bugzilla::Extension::LMBCustomFields::Util;
use Bugzilla::Error;

our $VERSION = '0.01';

# Declaring the custom fields to be created and their values
# name:         the name of the field in the database; begins with "cf_" if field is a custom field
# the "cf_" string will automatically be added at the beginning of the name of the custom field
# description:  a short string describing the field; displayed to Bugzilla users in several places within Bugzilla's UI
# type:         an integer specifying the kind of field this is; e.g. FREETEXT, SINGLE_SELECT, MULTI_SELECT
# fieldValues:  the values of the field to be populated by default
# For remaining properties, please refer to https://www.bugzilla.org/docs/tip/en/html/api/Bugzilla/Field.html
# The fields have to be defined in sequence for visibility_field to work.
my %customFields = (
  0 => {
    'name' => 'custom_1',
    'description' => 'Custom 1',
    'type' => 2,
    'fieldValues' => {
      0 => {'value' => 'ABC', 'sortkey' => '10'},
      1 => {'value' => 'DEF', 'sortkey' => '20'},
      2 => {'value' => 'XYZ', 'sortkey' => '30'},
    },
  },
  1 => {
    'name' => 'custom_2',
    'description' => 'Custom 2',
    'type' => 3,
    'visibility_field' => 'cf_custom_1',
    'visibility_values' => [1, 2],
    'fieldValues' => {
      0 => {'value' => 'ONE', 'sortkey' => '10'},
      1 => {'value' => 'TWO', 'sortkey' => '20'},
      2 => {'value' => 'THREE', 'sortkey' => '30'},
      3 => {'value' => 'FOUR', 'sortkey' => '40'},
    },
  },
);

# Calling the database insert hook of Bugzilla
sub install_update_db {
  my ($self, $args) = @_;
  
  my @fields = sort { $a <=> $b } values %customFields; # To make sure fields are created in order
  my $noOfFields = @fields;
  
  # Looping through the number of custom fields to be created
  for (my $i=0; $i < $noOfFields; $i++) {
    # Declaring variables for easier usage
    my $name = $fields[$i]{'name'};
    my $description = $fields[$i]{'description'};
    my $type = $fields[$i]{'type'};
    my $values = $fields[$i]{'fieldValues'};
    
    my $field = new Bugzilla::Field({ name => 'cf_' . $name });
    
    # Creating custom field if it has not been
    if (!defined $field) {
      my $fieldObj = {
        name              => $name,
        description       => $description,
        type              => $type,
        enter_bug         => 1,
        buglist           => 1,
        custom            => 1,
      };
      
      # For creating fields with visibility field control and values
      if (defined $fields[$i]{'visibility_field'} && defined $fields[$i]{'visibility_values'}) {
        my $visibility_field = $fields[$i]{'visibility_field'};
        my @visibility_values = @{$fields[$i]{'visibility_values'}};
      
        my $controlField = new Bugzilla::Field({ name => $visibility_field }); # Getting object of control field
        
        for (my $j=0; $j < scalar @visibility_values; $j++) { # Index of arrays starts from 0 but ID in database starts from 1. So we need to add 1 to the visibility_values which are defined above
          if ($controlField->{'type'} eq 2) { # If control field is SINGLE_SELECT, add 2. One for "---"
            $visibility_values[$j] = $visibility_values[$j] + 2;
          } else { # Otherwise, just add 1
            $visibility_values[$j] = $visibility_values[$j] + 1;
          }
        }

        $fieldObj = {
          name                => $name,
          description         => $description,
          type                => $type,
          enter_bug           => 1,
          buglist             => 1,
          custom              => 1,
          visibility_field_id => $controlField->id,
          visibility_values   => \@visibility_values,
        };
      }
      
      Bugzilla::Field->create($fieldObj);
    }
    
    # Re-instantiate the variable because the previous instantiation might end up with undefined value
    $field = new Bugzilla::Field({ name => 'cf_' . $name });
    
    if (defined $field && ($field->{'type'} eq 2 || $field->{'type'} eq 3)) { # the custom field is instantiated and type of the field is either SINGLE_SELECT or MULTI_SELECT
      my @fieldValues = sort { $a <=> $b } values $values; # To make sure field values are created in order
      my $noOfFieldValues = @fieldValues;
      
      # Populating field values into the custom field
      for(my $j=0; $j < $noOfFieldValues; $j++) {
        try {
          Bugzilla::Field::Choice->type($field)->create({ value => $fieldValues[$j]{'value'}, sortkey => $fieldValues[$j]{'sortkey'} });
          print "The value $fieldValues[$j]{'value'} created.\n";
        } catch {
          warn $_;
        };
      }
    }
  }
}

__PACKAGE__->NAME;